$(document).ready(function() {
    var a = skrollr.init({
    	forceHeight: false,
    	smoothScrolling: true
    });
    skrollr.menu.init(a,{
    	animate: true,
    	easing: "sqrt",
    	duration: function(b, c) {
            return 1500
        	}
    	}
    );

});